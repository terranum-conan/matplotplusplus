# Matplot++ #

## One step package creation

      conan create . terranum-conan+matplotplusplus/stable

## Upload package to gitlab

      conan upload matplotplusplus/1.1.0@terranum-conan+matplotplusplus/stable --remote=gitlab --all

## Build the debug package

      conan create . terranum-conan+matplotplusplus/stable -s build_type=Debug

Don't upload debug package on Gitlab.

## Step by step package creation

1. Get source code

        conan source . --source-folder=_bin
2. Create install files

        conan install . --install-folder=_bin --build=missing
3. Build

        conan build . --source-folder=_bin --build-folder=_bin
4. Create package 

        conan package . --source-folder=_bin --build-folder=_bin --package-folder=_bin/package
5. Export package

        conan export-pkg . terranum-conan+matplotplusplus/stable --source-folder=_bin --build-folder=_bin
6. Test package
      
        conan test test_package matplotplusplus/1.1.0@terranum-conan+matplotplusplus/stable


        