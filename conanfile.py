from conans import ConanFile, CMake, tools, AutoToolsBuildEnvironment
import os
import subprocess


class matplotplusplusConan(ConanFile):
    name = "matplotplusplus"
    description = "Matplot++"
    topics = ("conan", "Matplot++")
    url = "https://gitlab.com/terranum-conan/matplotplusplus"
    homepage = "https://github.com/alandefreitas/matplotplusplus"
    license = "MIT"
    version = "1.1.0"

    generators = ["cmake", "cmake_find_package"]
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "shared": [True, False],
        "build_examples": [True, False],
        "build_tests": [True, False],
    }
    default_options = {
        "shared": False,
        "build_examples": False,
        "build_tests": False,
    }

    _source_subfolder = "source_subfolder"
    _build_subfolder = "build_subfolder"

    def requirements(self):
        self.requires("libjpeg/9e")
        self.requires("libtiff/4.3.0")
        self.requires("zlib/1.2.12")
        self.requires("libpng/1.6.38")
        self.requires("openblas/0.3.20")
        self.requires("fftw/3.3.9")

    def source(self):
        tools.get(**self.conan_data["sources"][self.version], strip_root=True, destination=self._source_subfolder)

    def export_sources(self):
        self.copy("CMakeLists.txt")
        for patch in self.conan_data.get("patches", {}).get(self.version, []):
            self.copy(patch["patch_file"])

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions['BUILD_SHARED_LIBS'] = self.options.shared
        cmake.definitions['BUILD_EXAMPLES'] = self.options.build_examples
        cmake.definitions['BUILD_TESTS'] = self.options.build_tests

        cmake.configure(build_folder=self._build_subfolder)

        return cmake

    def _patch_sources(self):
        for patch in self.conan_data.get("patches", {}).get(self.version, []):
            tools.patch(**patch)

    def build(self):
        self._patch_sources()
        cmake = self._configure_cmake()
        cmake.build(target="matplotplusplus")

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
